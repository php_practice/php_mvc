<?php
/**
 * Created by PhpStorm.
 * User: Vikas VP
 * Date: 07-11-2016
 * Time: 11:13
 */

namespace Weekend\Controller;


use Symfony\Component\HttpFoundation\Response;
use Weekend\Service\TemplateService;
use Weekend\Service\ConfigService;

class BasicPageController
{

    protected $config;
    protected $theme;

    public function __construct(TemplateService $theme, ConfigService $config)
    {
        $this->theme = $theme;
        $this->config = $config;
    }

    public function get($path){
        $page = ($path == '/') ? 'index' : substr($path, 1);
        $menu = $this->config->getConfig();
        if(isset($menu[$page])){
            $content = $this->theme->render('basic.html', $menu[$page]);
            return Response::create($content);
        }
        return Response::create('NOt Found', 404);
    }
}