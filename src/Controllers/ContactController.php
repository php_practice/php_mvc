<?php
/**
 * Created by PhpStorm.
 * User: Vikas VP
 * Date: 07-11-2016
 * Time: 11:48
 */

namespace Weekend\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Weekend\Service\TemplateService;

class ContactController
{
    protected $theme;
  public function __construct(TemplateService $theme){
     $this->theme = $theme;
  }
  public function get(){

        $content = $this->theme->render('contact.html', [
            'title' => 'Contact',

        ]);
      return Response::create($content);
  }

  public function post(){
        $content = $this->theme->render('index.html',[
           'title' => 'Contact',
            'content' => 'Thank for the details',
        ]);
      return Response::create($content);
  }
}