<?php
/**
 * Created by PhpStorm.
 * User: Vikas VP
 * Date: 07-11-2016
 * Time: 15:11
 */

namespace Weekend\Service;


use League\Flysystem\FilesystemInterface;

class ConfigService
{
    protected $config;
    public function __construct(FilesystemInterface $fs)
    {
        $this->config = json_decode($fs->read('menu.json'), true);
    }

    public function getConfig(){
        return $this->config;
    }
}