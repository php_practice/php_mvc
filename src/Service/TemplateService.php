<?php
/**
 * Created by PhpStorm.
 * User: Vikas VP
 * Date: 08-11-2016
 * Time: 11:04
 * to seperate the template engine from the framework
 */

namespace Weekend\Service;

use Twig_Environment;

class TemplateService
{
    protected $twig;
    protected $menu;

    public function __construct(Twig_Environment $twig, ConfigService $config)
    {
        $this->twig = $twig;
        $this->menu = $config;
    }

    public function render($templateName, $variables){
        foreach ($this->menu as $path => $menuItem){
            $variables['nav'][] = [
                'title' => $menuItem['title'],
                'url' => '/'. $path,
            ];
        }
        return $this->twig->render($templateName, $variables);
    }
}